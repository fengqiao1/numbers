module FQ.NumeralNotation (
  IntegerNotation,
  getInt,
  fromInt
) where

import Prelude (Int)

class IntegerNotation a where
  getInt :: a -> Int
  fromInt :: Int -> a
