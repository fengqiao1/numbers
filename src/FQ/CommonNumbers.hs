module FQ.CommonNumbers where

-- nothing from Prelude
import Prelude ()
import FQ.Natural (Natural(Zero, Successor))
import FQ.Integer (natToInt, Integer)
import FQ.Rational (Rational(IntPair))

nat0 :: Natural
nat0 = Zero

nat1 :: Natural
nat1 = Successor nat0

nat2 :: Natural
nat2 = Successor nat1

int0 :: Integer
int0 = natToInt nat0

int1 :: Integer
int1 = natToInt nat1

int2 :: Integer
int2 = natToInt nat2

rational0 :: Rational
rational0 = IntPair int0 int1

rational1 :: Rational
rational1 = IntPair int1 int1

rational2 :: Rational
rational2 = IntPair int2 int1
