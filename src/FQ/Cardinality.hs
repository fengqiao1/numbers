module FQ.Cardinality where

import           Prelude                        ( Int
                                                , (*)
                                                )
import Data.List (isSubsequenceOf)

evenNumbers :: Int -> Int
evenNumbers x = x * 2

