module FQ.Arithmetic where

import FQ.Ring (Identity, HasZero, zero)

class Addition a where
  add :: a -> a -> a

class Multiplication a where
  multiply :: a -> a -> a

class Subtraction a where
  sub :: a -> a -> a

class Negation a where
  neg :: a -> a

class Reciprocal a where
  rec :: a -> a

class (Ord a, HasZero a, Negation a) => AbsoluteValue a where
  absVal :: a -> a
  absVal x | x < zero = neg x
           | otherwise = x
