module FQ.Natural
  ( Natural(..)
  , zeroNatural
  , oneNatural
  )
where

import           Prelude                        ( Eq
                                                , Ord
                                                , Ordering(EQ,LT,GT)
                                                , (==)
                                                , (+)
                                                , (-)
                                                , (.)
                                                , Show
                                                , show
                                                , compare
                                                , (++)
                                                , Bool(True, False)
                                                )
import           FQ.Arithmetic                  ( Addition
                                                , add
                                                , Multiplication
                                                , multiply
                                                )
import           FQ.NumeralNotation             ( IntegerNotation
                                                , getInt
                                                , fromInt
                                                )

data Natural = Zero | Successor Natural

zeroNatural :: Natural
zeroNatural = Zero

oneNatural :: Natural
oneNatural = Successor zeroNatural

instance Eq Natural where
  Zero          == Zero          = True
  _             == Zero          = False
  Zero          == _             = False
  (Successor n) == (Successor m) = n == m

instance Ord Natural where
  compare Zero          Zero          = EQ
  compare Zero          _             = LT
  compare _             Zero          = GT
  compare (Successor n) (Successor m) = compare n m

instance IntegerNotation Natural where
  getInt Zero          = 0
  getInt (Successor n) = getInt n + 1
  fromInt 0 = Zero
  fromInt x = Successor (fromInt (x - 1))

instance Addition Natural where
  add Zero          m = m
  add (Successor n) m = Successor (add n m)

instance Multiplication Natural where
  multiply Zero          _ = Zero
  multiply (Successor n) m = (n `multiply` m) `add` m

instance Show Natural where
  show = ("Natural " ++) . show . getInt
