module FQ.RationalSequence
  ( sqrt2
  )
where

import           Prelude                        ( Int
                                                , (-)
                                                , Bool(True, False)
                                                , (.)
                                                , ($)
                                                )
import           FQ.Rational                    ( Rational(IntPair)
                                                , oneRational
                                                )
import           FQ.Integer                     ( Integer )
import           FQ.Arithmetic                  ( multiply
                                                , add
                                                , rec
                                                )

--type RationalSeq = [Rational]

sqrt2 :: Int -> Rational
sqrt2 0 = oneRational
sqrt2 n = add oneRational . rec . add oneRational . sqrt2 $ n - 1
