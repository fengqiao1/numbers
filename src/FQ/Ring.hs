module FQ.Ring where

class Identity a where
  multId :: a
  addId :: a

class HasZero a where
  zero :: a
