module FQ.Rational
  ( Rational(IntPair)
  , zeroRational
  , oneRational
  , powerNatural
  , powerInteger
  )
where

import           Prelude                       as P
                                                ( Eq
                                                , (==)
                                                , Ord
                                                , show
                                                , otherwise
                                                , Show
                                                , compare
                                                , (++)
                                                , (>)
                                                , Ordering(EQ)
                                                )
import           FQ.Integer                     ( Integer
                                                , zeroInt
                                                , oneInt
                                                )
import           FQ.Arithmetic                  ( Multiplication
                                                , multiply
                                                , Addition
                                                , add
                                                , Negation
                                                , neg
                                                , Subtraction
                                                , sub
                                                , Reciprocal
                                                , rec
                                                , AbsoluteValue
                                                , absVal
                                                )
import           FQ.Ring                        ( HasZero
                                                , zero
                                                )

import           FQ.Natural                     ( Natural(Successor, Zero) )
import           FQ.NumeralNotation             ( IntegerNotation
                                                , getInt
                                                )

data Rational = IntPair Integer Integer

zeroRational :: Rational
zeroRational = IntPair zeroInt oneInt

oneRational :: Rational
oneRational = IntPair oneInt oneInt

instance Eq Rational where
  (IntPair a b) == (IntPair c d) = multiply a d == multiply c b

instance Addition Rational where
  add (IntPair a b) (IntPair c d) = IntPair e f
   where
    e = add (multiply a d) (multiply b c)
    f = multiply b d

instance Multiplication Rational where
  multiply (IntPair a b) (IntPair c d) = IntPair (multiply a c) (multiply b d)

instance Negation Rational where
  neg (IntPair a b) = IntPair (neg a) b

instance Subtraction Rational where
  sub a b = add a (neg b)

instance Reciprocal Rational where
  rec (IntPair a b) = IntPair b a

instance P.Ord Rational where
  compare x@(IntPair a b) y@(IntPair c d)
    | x == y       = EQ
    | c == zeroInt = compare (multiply a b) zeroInt
    | a == zeroInt = compare zeroInt (multiply c d)
    | otherwise    = compare (sub x y) zeroRational

instance HasZero Rational where
  zero = zeroRational

instance AbsoluteValue Rational

-- Exponentiation
powerNatural :: Rational -> Natural -> Rational
powerNatural _ Zero          = oneRational
powerNatural x (Successor n) = multiply (powerNatural x n) x

powerInteger :: Rational -> Integer -> Rational
powerInteger x n | n == zeroInt = oneRational
                 | n > zeroInt  = multiply (powerInteger x (sub n oneInt)) x
                 | otherwise    = rec (powerInteger x (neg n))

-- Show
instance Show Rational where
  show (IntPair a b) =
    show (getInt a) ++ "//" ++ show (getInt b)

