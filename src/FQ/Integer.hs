module FQ.Integer
  ( Integer(NaturalPair)
  , natToInt
  , zeroInt
  , oneInt
  )
where

import           Prelude                       as P
                                                ( Eq
                                                , Show
                                                , (==)
                                                , (-)
                                                , (++)
                                                , Ord
                                                , show
                                                , compare
                                                , (>=)
                                                , otherwise
                                                )
import           FQ.Natural                     ( Natural(Zero, Successor) )
import           FQ.Arithmetic                  ( Addition
                                                , add
                                                , Multiplication
                                                , multiply
                                                , Subtraction
                                                , sub
                                                , Negation
                                                , neg
                                                )
import           FQ.NumeralNotation             ( IntegerNotation
                                                , getInt
                                                , fromInt
                                                )

data Integer = NaturalPair Natural Natural

natToInt :: Natural -> Integer
natToInt n = NaturalPair n Zero

instance Eq Integer where
  (NaturalPair a b) == (NaturalPair c d) = add a d == add c b

instance IntegerNotation Integer where
  getInt (NaturalPair a b) = getInt a - getInt b
  fromInt x | x >= 0    = natToInt (fromInt x)
            | otherwise = NaturalPair Zero (fromInt (-x))

instance P.Show Integer where
  show (NaturalPair a b) =
    "Integer (" ++ show (getInt a) ++ "," ++ show (getInt b) ++ ")"

instance Addition Integer where
  add (NaturalPair a b) (NaturalPair c d) = NaturalPair (add a c) (add b d)

instance Multiplication Integer where
  multiply (NaturalPair a b) (NaturalPair c d) = NaturalPair e f
   where
    e = add (multiply a c) (multiply b d)
    f = add (multiply a d) (multiply b c)

instance Negation Integer where
  neg (NaturalPair a b) = NaturalPair b a

instance Subtraction Integer where
  sub a b = add a (neg b)

instance P.Ord Integer where
  compare (NaturalPair a b) (NaturalPair c d) = compare (add a d) (add c b)

-- Common numbers
zeroInt :: Integer
zeroInt = natToInt Zero

oneInt :: Integer
oneInt = natToInt (Successor Zero)
