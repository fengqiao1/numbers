.PHONY: clean pdf

clean:
	rm build/*

pdf:
	cp ./doc/*.bib ./build/
	latexmk --pdf -pvc ./doc/intro.tex -outdir=./build
