module TestNatural where

import           Test.Hspec

main :: IO ()
main = hspec $
  describe "Natural number" $ do
    it "2 equals 2" $
      shouldBe 2 2
    it "1 not equal 0" $
      shouldNotBe 1 0
