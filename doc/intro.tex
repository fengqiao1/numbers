\documentclass{article}

\usepackage[utf8]{inputenc}

\title{Construction of Real Number without Bit Operations}
\author{Feng Qiao}
\date{June 29, 2020}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{xcolor}
\usepackage{courier}
\usepackage{hyperref}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    keepspaces=true,
    numbers=left,
    numbersep=5pt,
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    language=Haskell
}

\lstset{style=mystyle}

\newcommand{\ds}{\displaystyle}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\atb}{a \times b}
\newcommand{\seq}[1]{({#1}_t)^{\infty}_{t=0}}

\begin{document}

\maketitle

\section{Introduction}\label{sec:intro}
Almost all modern computer devices rely on binary numeral system because it is the way logic gates work.
Numbers in computer are represented with bits and operations such as addition and multiplication are implemented
with logic operations. For instance, in C language, integer can be "int" with 1 sign bit and 15 magnitude bits;
there is also "unsigned long" with 32 bits for positive integers that are less than $2^{32}$.

This essay tries to construct a real number system on a software level. Numbers are constructed with objects and mappings
(functions) without relying on predefined bitwise operations.

This project is purely theoretical. Since recursion are widely used in analysis, the computational
complexity is significantly greater than bitwise operations.

Although haskell is chosen here, the concepts are language agnostic.

The source code is hosted on GitLab: \url{https://gitlab.com/fengqiao1/numbers}

\section{Natural Number}\label{sec:natural}
We start with a basic question: what do we need to create a number system? Do we have to exhaust all integers to define
$\mathbb{Z}$?

We define a start point, an object, and name it "zero". Then recursively, the object succeeds "zero" is called "one".
The iteration goes to as big a number as it could be. So there are two constructors of natural number: "zero" that does
not need an argument and "Successor" with its predecessor as argument.

\begin{lstlisting}
data Natural = Zero | Successor Natural
\end{lstlisting}

Then how are two naturals equal? We define zero to be equal to zero, and two natural to be equal if their predecessors are equal.
\begin{lstlisting}
instance Eq Natural where
  Zero == Zero = True
  _ == Zero = False
  Zero == _ = False
  (Successor n) == (Successor m) = n == m
\end{lstlisting}

We define addition recursively:
\begin{equation*}
  \begin{aligned}
    0 + m &:= m \\
    (n++)+m &:= (n+m)++
  \end{aligned}
\end{equation*}
That is:
\begin{lstlisting}
instance Addition Natural where
  add Zero m = m
  add (Successor n) m = Successor (add n m)
\end{lstlisting}
In the same manner, multiplication is defined:
\begin{equation*}
  \begin{aligned}
    0 \times m &:= 0 \\
    (n++) \times m &:= (n \times m)+ m
  \end{aligned}
\end{equation*}
\begin{lstlisting}
instance Multiplication Natural where
  multiply Zero _ = Zero
  multiply (Successor n) m = (n `multiply` m) `add` m
\end{lstlisting}
Verify that $(2 \times 3) \times 4 = 2 \times (3 \times 4)$.
\begin{lstlisting}
>let nat0 = Zero
>let nat1 = Successor nat0
>let nat2 = Successor nat1
>let nat3 = Successor nat2
>let nat4 = Successor nat3
>multiply (multiply nat2 nat3) nat4
Natural 24
>multiply nat2 (multiply nat3 nat4)
Natural 24
\end{lstlisting}

\subsection{Proof 1: Multiplication is associative}\label{subsec:proofone}
For any natural numbers $a,b,c$, we have $(\atb) \times c = a \times (b \times c)$.

\textbf{Proof:}

Fix $a$ and $b$.
When $c=0$, because multiplication is commutative, \[(\atb) \times 0 = 0 \times (\atb)=0\]
\[a \times (b \times 0) = a \times (0 \times b) = a \times 0 =0\]
The statement is true when $c=0$.

Suppose the statement is true when $c=n$. That is,
\[(\atb) \times n = a \times (b \times n)\]
When $c=n++$, \[(\atb) \times (n++) = (n++) \times (\atb) = (n\times (a\times b)) + (a\times b)\]
\[a \times (b \times (n++)) = a \times ((n \times b) + b)\]
By distributive law,
\begin{equation*}
  \begin{aligned}
    a \times ((n \times b) + b) &= (a \times (n \times b))+(\atb)\\
    &= (a \times (b \times n)) + (\atb)\\
    &= ((\atb) \times n) + (\atb)\\
    &= (n \times (\atb)) + (\atb)
  \end{aligned}
\end{equation*}
Then we have $(\atb) \times (n++) = a \times (b \times (n++))$. The statement is true when $c=n++$.
Therefore, for any natural number $c$, we have $(\atb) \times c = a \times (b\times c)$.
We can also induct on $a,b$ in the same way. Multiplication is associative for all natural numbers.

\section{Integers and Rationals: Countable}\label{sec:rational}
Since we want more than addition and multiplication, we have to find a way to define the ``reverse" actions.
In the book, dashes and slashes are used. In programming languages, the formal objects can be a tuple, list,
or any data structure as long as it stores two objects.
\begin{lstlisting}
data Integer = NaturalPair Natural Natural
data Rational = IntPair Integer Integer
\end{lstlisting}

We then define subtraction and division with addition and multiplication.
\begin{lstlisting}
instance Negation Integer where
  neg (NaturalPair a b) = NaturalPair b a

instance Subtraction Integer where
  sub a b = add a (neg b)

instance Reciprocal Rational where
  rec (IntPair a b) = IntPair b a
\end{lstlisting}
We are quite comfortable with integers since they resemble natural numbers;
but can we iterate over the rational? Is there an explicit way?

\subsection{Proof 2: Rational numbers are countable.}
Prove rational numbers are countable by show there is a bijection $f:\mathbb{N}\ra \mathbb{Q}$.

\textbf{Proof:}

For $x\in \mathbb{N}$, first, map $x$ to the set $s_x=\{i\in \mathbb{N}|i < x\}$.
\begin{lstlisting}
oneToX :: Int -> [Int]
oneToX x = [1 .. x-1]
\end{lstlisting}
\begin{lstlisting}
>let arr = [1,2,3]
>let arr2 = map oneToX arr
[[],[1],[1,2]]
\end{lstlisting}
Then, for $j\in s_x$, map to two rationals: $j//x$ and $x//j$
\begin{lstlisting}
[[],[1//2,2//1],[1//3,3//1,2//3,3//2]]
\end{lstlisting}
We can also have the negative mapping $j//-x$, $-x//j$.
\begin{lstlisting}
[[],[1//-2,-2//1],[1//-3,-3//1,2//-3,-3//2]]
\end{lstlisting}
Combine the positve array, negative array, $0//1$ and $1//1$, we have the array $S$
that maps $\mathbb{N}\ra\mathbb{Q}$.
\begin{lstlisting}
>take 20 rationalList
[0//1,1//1,-1//1,1//2,1//-2,2//1,-2//1,1//3,1//-3,3//1,-3//1,
2//3,2//-3,3//2,-3//2,1//4,1//-4,4//1,-4//1,2//4]
\end{lstlisting}
Then for any rational $x=p/q$, $x=1//1 \in S$ when $p=q$ and $x\in S$ when $p<q$ or $p>q$. Thus, rationals are countable.

Though rationals are infinite,
the point of countable is that we can explicitly enumerate the rationals
generated by any fiinite $x\in \mathbb{N}$, so we can write programs that can end.

\subsection{Proof 3: $\sqrt{2}$ is irrational}
Can we make $\sqrt{2} =p/q$?

We can start with $1 +u=\sqrt{2}$. We know $1+ 1/2 > \sqrt{2}$ since $(3/2)^2 > 2$.
To get closer, we can increase the denominator by some rational $u_0$ \[1+\frac{1}{2+u_0}\]
This is the idea of continued fraction.
\[1+\frac{1}{2+ \cfrac{1}{2+ \cfrac{1}{2+ ...} } }\]
It can be written as a recursive function $a_n=1 + 1/(1+a_{n-1})$, let $a_0=1$
\begin{lstlisting}
>sqrt2 1
3//2
>sqrt2 2
7//5
>sqrt2 3
17//12
>sqrt2 4
41//29\end{lstlisting}
Because continued fraction expansion is finite iff the number is rational\cite{hmcf}, we want to prove that
$\sqrt{2}$ is not finite expansion.

\textbf{Proof:}

Suppose $\ds\sqrt2 = 1 + \frac{a_0}{b_0}$ where $a_0,b_0$ are some integers.
Then $\ds \frac{a_0}{b_0}>0$, which means $a_0 b_0>0$. Thus, it is either $a_0,b_0>0$ or $a_0,b_0<0$.
We can always make $a_0,b_0>0$ (choose $\frac{-a_0}{-b_0}=\frac{a_0}{b_0}$ when $a_0,b_0<0$).

Since $b_0>a_0$, by Euclidean algorithm, $b_0=p_0 a_0 + r_1$ for some positive integers (naturals) $p_0,r_1$ and $0<r_1<a_0$.

Keep doing Euclidean algorithm, we can end at some finite $n$.
\begin{equation*}
  \begin{aligned}
    a_0 &=p_1r_1+r_2\\
    r_1 &=p_2r_2+r_3\\
    r_2 &=p_3r_3+r_4\\
    \vdots \\
    r_{n-1} &=p_{n-1}r_n +0
  \end{aligned}
\end{equation*}
Then the continued fraction is
\begin{equation*}
  \begin{aligned}
    1+ \frac{a_0}{b_0} &= 1+\frac{1}{p_0+ \frac{r_1}{a_0} }\\
    &= 1+\cfrac{1}{p_0 + \cfrac{1}{p_1 + \cfrac{1}{\ddots +\cfrac{1}{p_{n-1}}}}}
  \end{aligned}
\end{equation*}
Rewrite the fraction $[p_0,...,p_{n-1}]$ as a recursive function:
\begin{equation*}
  p'(k) = \left\{
        \begin{array}{ll}
          p_{n-1} & \quad k =0 \\
            p_{n-k} + \frac{1}{p'(k-1)} & \quad 0<k\leq n
        \end{array}
    \right.
\end{equation*}

When $k=0$, $p'(0)=p_{n-1} \neq \sqrt2$ since there is no $x\in \mathbb{N}$ such that $x^2=2$.
We also have $p'(0)-1 \neq \sqrt2$.

Suppose when $k=m$ ($m\in \mathbb{N}$), $p'(m) \neq \sqrt2 $ and $\sqrt2 +1$; when $k=m+1$,
\begin{equation*}
  p'(m+1) = p_{n-m} + \frac{1}{p'_m}
\end{equation*}
When $p_{n-m}\geq 2$, $p'(m+1)>\sqrt2$; when $p_{n-m}=1$,
\begin{equation*}
  p'(m+1)-\sqrt2 = 1 + \frac{1}{p'(m)}-\sqrt2 = \frac{1}{p'(m)}- \frac{1}{1+\sqrt2}
\end{equation*}
Since $p'(m) \neq \sqrt2+1$, $p'(m+1)- \sqrt2\neq 0$.

Therefore, $1+ \frac{a_0}{b_0} \neq \sqrt2$. $\sqrt2$ is not a finite continued fraction.



\section{Real number: Uncountable and Infinity}
The proof that $\sqrt2$ is not rational shows that we are not able to get to the ``perfect exact" $\sqrt2$,
even though we can get very very very close with integers.
We have defined naturals, integers and rationals; any element of those numbers is a combination of objects,
more importantly, finite combination. $2=(0++)++$, ``one third"$=1//3$.

We have ``infinite" numbers, so are they enough? Of course not. We define real number to be a Cauchy sequence.
The difference is that it is an infinite list, which means, if we write a program to get the value,
it will run forever. To evaluate a real, we have to specify a level.
Stop at $n=100$ or one million, and we will lose precision.
The good news is that Cauchy sequence guarantees that the elements will stay close to each other ``eventually";
after some point, the precision lost is not wild. We can drop the first 100 or ten million if we like.

\subsection{Proof 4: Real numbers are uncountable}
Real numbers are infinite, but can we enumerate them like rationals?

Prove there is no bijection $f:\mathbb{N}\ra \mathbb{R}$.

\textbf{Proof:}

Suppose we can assign each real number in $a\in[1,0]$ to a natural.
By well ordering principle, we can sort the real numbers $f(n)$ in an increasing order.

Pick $f(k)$ and $f(k+1)$ where $k\in \mathbb{N}$, let $f(k)=\seq{a}$ and $f(k+1)=\seq{b}$. By the definition of Cauchy sequence,
there is some $\epsilon_1>0$ such that, for any natural $N$, there are some
$|a_k-b_k|>\epsilon_1$ where $k>N$.

Construct a new sequence $\seq{c}=((a_t+b_t)/2)^{\infty}_{t=0}$.
For any $\epsilon_2>0$, there exist some $N_1$ and $N_2$ such that for any $h,j>N_1$ and $h,j>N_2$,
\[|a_h-a_j|<\epsilon_2/100 \quad |b_h-b_j|<\epsilon_2/100\]
Let $N_3=max\{N_1,N_2\}$, then for any $h,j>N_3$,
\begin{equation*}
  \begin{aligned}
    |c_h-c_j| &= \left|\frac{a_h+b_h}{2} - \frac{a_j+b_j}{2}\right|\\
    &= \frac{1}{2} |(a_h-a_j)+(b_h-b_j)|\\
    &< \frac{1}{2} (\epsilon_2/100 + \epsilon_2/100)\\
    &< \epsilon_2
  \end{aligned}
\end{equation*}
Thus, $\seq{c}$ is a Cauchy sequence; $\seq{c}$ is a real number.

Because $f(k)\neq f(k+1)$, there exists some $\epsilon_3>0$ such that for any $N>0$,
there are some $|a_i-b_i|>\epsilon_3$ where $i>N$. Then
\begin{equation*}
  \begin{aligned}
    |a_i-c_i| &= |a_i - \frac{a_i+b_i}{2}|\\
    &= \frac{1}{2} |a_i-b_i|\\
    &> \frac{1}{2}\epsilon_3
  \end{aligned}
\end{equation*}
In the same way, $|c_i-b_i|>\frac{1}{2}\epsilon_3$.
Thus $\seq{c}\neq \seq{a}$ and $\seq{c}\neq \seq{b}$. Then $\seq{c}$ is new real number;
since $\seq{a}<\seq{c}<\seq{b}$, $\seq{c}\neq f(n)$ for any $n\in \mathbb{N}$.
It contradicts $f$ is a bijection. There is no bijection $f:\mathbb{N}\ra\mathbb{R}$.

\medskip

The proof shows that real number is so ``dense" that, given any two elements, there are infinite elements between them.
If we write a program to print all reals, it will take forever to even go from 1 to 1.0000001.

\subsection{Proof 5: Infinite cardinality}
An interesting question emerges when we talk about infinite sets. It seems that real number has more elements that
rational number and integer is twice as big as natural number.

But Hilbert's paradox of grand hotel shows that even integer has the same size of integer even though it is a subset;
and Banach–Tarski Paradox creates a new copy from a set.
Only when we know there are always hotel rooms left can we keep reassigning without ``running out".

\medskip

Prove a set $A$ is infinite if there exists an injection $f:A\ra B$ where $B\subset A$.

\textbf{Proof:}

Since $B\subset A$, there is some $x\in A$ and $x\notin B$. Suppose $A$ is finite.
Then $\#(A)>\#(B)$.

Because $f$ is injective, for every $x,y\in A$, $f(x)\neq f(y)$. Then $\#(A)\leq\#(B)$.

Therefore, $A$ can not be an infinite set.

\medskip

Basically, Hilbert's paradox gives a method to establish an injection from a "bigger" set to a smaller one.




\bibliographystyle{plain}
\bibliography{references}
\end{document}
